
# hi5.agency FE code challenge. 
Using [dog-api ](https://dog.ceo/dog-api/) + React or Vue, please build a simple SPA listing out the dog breeds. 
Breed list items should be clickable and should direct you to an image of a dog for the breed user clicked on. 

Please clone and cut a new branch.

## REQUIERMENTS

 - Pull data (whichever method you prefer)
 - Setup as many components as needed.
 - Demonstrate state management.
 - Responsive layout.
 - Updated readme with instructions for local setup.

Any questions feel free to reach Rudy at rquevedo@hi5.agency